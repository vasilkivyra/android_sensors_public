package com.hci.sensoric.network;


import java.nio.ByteBuffer;

/**
 * Created by yurii on 14/11/17.
 */

public class message_header_protocol
{

	private static final String HEADER_KEYWORD = "LENGTH=";
	private static final short HEADER_KEYWORD_LENGTH = 7;
	private static final short HEADER_BODY_LENGTH_LENGTH = 4;
	private static short HEADER_SIZE = 11;

	public static byte[] insert_header(final byte [] data)
	{

		byte size[] = new byte[4];
		size = intToByteArray(data.length);


		ByteBuffer buf = ByteBuffer.allocate(11 + data.length);

		buf.put(HEADER_KEYWORD.getBytes());
		buf.put(size);
		buf.put(data);

		return buf.array();

	}


	public static boolean is_header_ok(final byte[] header)
	{
		if (header.length != HEADER_SIZE)
			throw new RuntimeException("Invalid header size!");

		return new String(header, 0, HEADER_KEYWORD_LENGTH).equals(HEADER_KEYWORD);
	}

	public static int extract_body_length(final byte[] header)
	{
		if (header.length != HEADER_SIZE)
			throw new RuntimeException("Invalid header size!");

		return byteArrayToInt(new String(header,
				HEADER_KEYWORD_LENGTH,
				HEADER_BODY_LENGTH_LENGTH).getBytes());
	}


//    byte[] intToByteArray(int value) {
//        return  ByteBuffer.allocate(4).putInt(value).array();
//    }

	public static byte[] intToByteArray(int value)
	{
		return new byte[]{
				(byte) (value >> 24),
				(byte) (value >> 16),
				(byte) (value >> 8),
				(byte) value};
	}


//    int byteArrayToInt(byte[] bytes) {
//        return ByteBuffer.wrap(bytes).getInt();
//    }

	public static String IntToByteArrayToStr(int value)
	{
		byte [] array = new byte[]{
				(byte) (value >> 24),
				(byte) (value >> 16),
				(byte) (value >> 8),
				(byte) value};
		return new String(array, 0 , array.length);
	}


	// packing an array of 4 bytes to an int, big endian
	public static int byteArrayToInt(byte[] bytes)
	{
		return bytes[0] << 24 |
				(bytes[1] & 0xFF) << 16 |
				(bytes[2] & 0xFF) << 8 |
				(bytes[3] & 0xFF);
	}

	public static int strToByteArrayToInt(String data)
	{
		byte [] bytes = data.getBytes();
		return   bytes[0] << 24 |
				(bytes[1] & 0xFF) << 16 |
				(bytes[2] & 0xFF) << 8 |
				(bytes[3] & 0xFF);
	}

	public static String byteArrayToIntToStr(byte[] bytes)
	{
		return String.valueOf(bytes[0] << 24
				| (bytes[1] & 0xFF) << 16
				| (bytes[2] & 0xFF) << 8
				| (bytes[3] & 0xFF));
	}

}
