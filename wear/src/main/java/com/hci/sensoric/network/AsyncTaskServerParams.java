package com.hci.sensoric.network;

import java.net.Socket;

/**
 * Created by yurii on 18/11/17.
 */

public class AsyncTaskServerParams
{
	public ConnectionHandler callback;
	public Socket socket;

	AsyncTaskServerParams(Socket socket, ConnectionHandler callback) {
		this.socket = socket;
		this.callback = callback;
	}
}