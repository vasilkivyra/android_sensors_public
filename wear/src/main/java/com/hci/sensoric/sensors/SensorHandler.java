package com.hci.sensoric.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by yurii on 17/11/17.
 */

public class SensorHandler implements SensorEventListener
{
	private final float[] m_acc = new float[3];
	private final float[] m_gyro = new float[3];
	private final float[] m_magnet = new float[3];
	// corrected_roll, corrected_pitch, corrected_yaw;
	private long[] m_last_measurement = {-5,-5,-5};
	private long[] m_current_measurements = new long[3];
	private float orientation[] = new float[3];
	private float rotation_matrix[] = new float[9];
	private float inclination_matrix[] = new float[9];
	private long m_interval = 0;
	private boolean m_isStopped = true;

	private AtomicBoolean m_success = new AtomicBoolean(false);
	private SensorManager m_sensor_manager;
	private Sensor m_accelerometer;
	private Sensor m_magnetic;
	private Sensor m_gyroscope;


	private LowPassFilter filter_yaw = new LowPassFilter(0.06f);
	private LowPassFilter filter_pitch = new LowPassFilter(0.03f);
	private LowPassFilter filter_roll = new LowPassFilter(0.03f);

	private ArrayList<SimpleListener<long[]>> m_subscribers = new ArrayList<>();

	public SensorHandler(Context context)
	{
		m_sensor_manager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		if (m_sensor_manager == null)
			throw new RuntimeException("SensorManager is null!");

		m_accelerometer = m_sensor_manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		m_magnetic = m_sensor_manager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		m_gyroscope = m_sensor_manager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

		m_interval = 0;
	}

	public SensorHandler(Context context, long update_every)
	{
		m_sensor_manager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		if (m_sensor_manager == null)
			throw new RuntimeException("SensorManager is null!");

		m_accelerometer = m_sensor_manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		m_magnetic = m_sensor_manager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		m_gyroscope = m_sensor_manager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

		m_interval = update_every;
	}

	public void setInterval(long update_every)
	{
		m_interval = update_every;
	}

	public void subscribe(SimpleListener<long[]> callback)
	{
		m_subscribers.add(callback);
	}

	public void unsubscribe(SimpleListener<long[]> callback)
	{
		boolean result = m_subscribers.remove(callback);
		Log.d("Subscribers: ", "" + m_subscribers.size() + ", remove() = " + result);
	}

	public boolean hasSubscribers()
	{
		return m_subscribers.size() != 0;
	}

	public void stop()
	{
		Log.d("call: ", "stop()");
		m_isStopped = true;
		m_sensor_manager.unregisterListener(this, m_accelerometer);
		m_sensor_manager.unregisterListener(this, m_magnetic);
		m_sensor_manager.unregisterListener(this, m_gyroscope);
	}

	public void start()
	{
		Log.d("call: ", "start()");
		m_isStopped = false;
		m_sensor_manager.registerListener(this, m_accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
		m_sensor_manager.registerListener(this, m_magnetic, SensorManager.SENSOR_DELAY_NORMAL);
		m_sensor_manager.registerListener(this, m_gyroscope, SensorManager.SENSOR_DELAY_NORMAL);
	}

	public long[] get_XYZ_angle_values()
	{
		m_success.set(false);
		while (!m_success.get()) ;
		return m_current_measurements;
	}

	private long diff(long[] current_measurement, long[] last_measurement)
	{
		// so far we are only interested in Z-axis. So update happens only when
		// z changes to interval value
		return (Math.abs(Math.abs(current_measurement[0]) - Math.abs(last_measurement[0])));
	}

	@Override
	public void onSensorChanged(SensorEvent event)
	{
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
			System.arraycopy(event.values, 0, m_acc, 0, 3);   // save data
		if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
			System.arraycopy(event.values, 0, m_magnet, 0, 3);   // save data

		if (m_acc != null && m_magnet != null)
		{
			if (!SensorManager.getRotationMatrix(rotation_matrix, inclination_matrix, m_acc, m_magnet))
			{
				Log.e("onSensorChanged", "getRotationMatrix failure.");
				return;
			}

			SensorManager.getOrientation(rotation_matrix, orientation);

			m_current_measurements[0] = (int) filter_yaw.lowPass(Math.toDegrees(orientation[2]));
			m_current_measurements[1] = (int) filter_pitch.lowPass(Math.toDegrees(orientation[1]));
			m_current_measurements[2] = (int) filter_roll.lowPass(Math.toDegrees(orientation[0]));

			m_success.set(true);
			if (m_current_measurements[0] == m_last_measurement[0])
				return;

			for (SimpleListener<long[]> callback : m_subscribers)
					callback.update(m_current_measurements);

			System.arraycopy(m_current_measurements, 0, m_last_measurement, 0, 3);
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy)
	{
		// Nothing important to do
	}


	public boolean isStopped()
	{
		return m_isStopped;
	}
}

