package com.hci.sensoric.sensors;

/**
 * Created by yurii on 06/11/17.
 */

public class LowPassFilter
{
	/*
	 * time smoothing constant for low-pass filter 0 ≤ alpha ≤ 1 ; a smaller
	 * value basically means more smoothing See: http://en.wikipedia.org/wiki
	 * /Low-pass_filter#Discrete-time_realization
	 */
	private float ALPHA = 0f;
	private double lastOutput = 0;

	public LowPassFilter(float alpha)
	{
		this.ALPHA = alpha;
	}

	public double lowPass(double input)
	{
		if (Math.abs(input - lastOutput) > 170)
		{
			lastOutput = input;
			return lastOutput;
		}

		lastOutput = lastOutput + ALPHA * (input - lastOutput);
		return lastOutput;
	}
}
