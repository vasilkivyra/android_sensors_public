package com.hci.sensoric.network;

import android.util.Log;

import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.util.Arrays;

import static com.hci.sensoric.network.message_header_protocol.IntToByteArray;
import static com.hci.sensoric.network.message_header_protocol.byteArrayToInt;

/**
 * Created by yurii on 17/11/17.
 */

public class Protocol
{
	private static final int ERROR = 0;
	private static final int SUCCESS = 1;

	public Protocol()
	{

	}

	public byte[] generate_error(Message request_message)
	{
		ByteBuffer buf = ByteBuffer.allocate(4 + 4 + 4 + request_message.err.message.length());
		buf.put(IntToByteArray(Message.Type.RESPONSE.value));
		buf.put(IntToByteArray(request_message.err.code));
		buf.put(IntToByteArray(request_message.err.message.length()));
		buf.put(request_message.err.message.getBytes());
		return buf.array();
	}

	public byte[] generate_wrist_orientation_update_response(long[] values)
	{
		ByteBuffer buf = null;
		try
		{

			int body_size = 0;

			for (long value : values)
			{
				body_size += 4 + String.valueOf(value).length();
			}

			buf = ByteBuffer.allocate(4 + 4 + 4 + body_size);

			buf.put(IntToByteArray(Message.Type.RESPONSE.value));
			buf.put(IntToByteArray(Message.Status.OK.value));
			buf.put(IntToByteArray(Message.Event.WRIST_ORIENTATION_UPDATE.value));
			for (long value : values)
			{
				buf.put(IntToByteArray(String.valueOf(value).length()));
				buf.put(String.valueOf(value).getBytes());
			}

			return buf.array();
		}
		catch (BufferOverflowException boe)
		{
			Log.d("before Exception:", "" + buf.array().length);
			boe.printStackTrace();
			return null;
		}
	}

	public byte[] generate_get_rotation_angle_response(long[] values)
	{
		ByteBuffer buf = null;
		try
		{

			int body_size = 0;

			for (long value : values)
			{
				body_size += 4 + String.valueOf(value).length();
			}

			buf = ByteBuffer.allocate(4 + 4 + body_size);

			buf.put(IntToByteArray(Message.Type.RESPONSE.value));
			buf.put(IntToByteArray(Message.Status.OK.value));
			//buf.put(IntToByteArray(Message.Event.WRIST_ORIENTATION_UPDATE.value));
			for (long value : values)
			{
				buf.put(IntToByteArray(String.valueOf(value).length()));
				buf.put(String.valueOf(value).getBytes());
			}

			return buf.array();
		}
		catch (BufferOverflowException boe)
		{
			Log.d("before Exception:", "" + buf.array().length);
			boe.printStackTrace();
			//System.exit(1);
			return null;
		}
	}

	private int extract_status(byte[] data)
	{
		return byteArrayToInt(data);
	}

	private Message.Type extract_type(byte[] data)
	{
		int res = byteArrayToInt(Arrays.copyOfRange(data, 0, 4));

		if (res == Message.Type.REQUEST.value)
			return Message.Type.REQUEST;

		else if (res == Message.Type.RESPONSE.value)
			return Message.Type.RESPONSE;

		else
			return Message.Type.INVALID;
	}

	private Message.Method extract_method(byte[] request)
	{
		int method = byteArrayToInt(Arrays.copyOfRange(request, 4, 8));

		if (method == Message.Method.SingleGetRotationAngleEvent.value)
			return Message.Method.SingleGetRotationAngleEvent;
		else if (method == Message.Method.SubscribeForRotationAngleEvents.value)
			return Message.Method.SubscribeForRotationAngleEvents;
		else if (method == Message.Method.UnSubscribeForRotationAngleEvents.value)
			return Message.Method.UnSubscribeForRotationAngleEvents;
		else if (method == Message.Method.ShortVibration.value)
			return Message.Method.ShortVibration;
		else if (method == Message.Method.LongVibration.value)
			return Message.Method.LongVibration;
		else
			return Message.Method.DoesNotExist;
	}


	public Message extract_message(byte[] data)
	{
		Message message = new Message();
		message.type = extract_type(data);

		switch (message.type)
		{
			case REQUEST:
				handle_request(data, message);
				break;
			case RESPONSE:
				handle_response(data, message);
				break;
			case INVALID:
				message.status = Message.Status.ERROR;
				message.err.code = Message.ErrorCode.INVALID_TYPE.value;
				message.err.message = Message.ErrorCode.INVALID_TYPE.name();
				break;
		}

		return message;
	}

	private void handle_request(byte[] data, Message message)
	{
		message.method = extract_method(data);

		if (message.method == Message.Method.DoesNotExist)
		{
			message.status = Message.Status.ERROR;
			message.err.code = Message.ErrorCode.INVALID_METHOD.value;
			message.err.message = Message.ErrorCode.INVALID_METHOD.name();
			return;
		}

		message.status = Message.Status.OK;

		// parsing/extracting request arguments
		byte[] data_arguments = Arrays.copyOfRange(data, 8, data.length);
		int begin_position = 0;
		int end_position = 4;
		int data_arg_size;
		byte[] argument;

		if (data.length > 8)
			while (true)
			{

				data_arg_size = byteArrayToInt(Arrays.copyOfRange(data_arguments,
						begin_position,
						end_position));
				begin_position = end_position;
				end_position = begin_position + data_arg_size;

				argument = Arrays.copyOfRange(data_arguments, begin_position, end_position);
				message.arguments.add(new String(argument));

				begin_position = end_position;
				end_position += 4;

				if (begin_position >= data_arguments.length)
					break;
			}

		// For future, if ever any request has parameters
		// We are gonna be switching method type and depends on the
		// method type we are gonna be assigning request params on
		// te correct object.
		// e.g. if method SubscribeForRotationAngleEvents has bool parameter X
		// then we are gonna have a structure like that
		// public class SubscribeForRotationAngleEventsMethod
		// {
		//		bool x;
		// }
		//
		// In switch statement if message.method = Message.Method.SubscribeForRotationAngleEvents
		// then we will do the following:
		//
		// case SubscribeForRotationAngleEvents:
		// 		message.subscribeForRotationAngleEvents.x = (message.arguments[0]);
		// break;
		switch (message.method)
		{
			case SubscribeForRotationAngleEvents:
				message.subscribeForRotationAngleEventsMethod.task_id = message.arguments.get(0);
				message.subscribeForRotationAngleEventsMethod.port = Short.parseShort(message.arguments
						.get(1));
				break;
			case UnSubscribeForRotationAngleEvents:
				message.unsubscribeForRotationAngleEventsMethod.task_id = message.arguments.get(0);
				break;
			case SingleGetRotationAngleEvent:
				break;
			case DoesNotExist:
				break;
		}
	}

	private void handle_response(byte[] data, Message message)
	{
		int status = extract_status(data);
		switch (message.status)
		{
			case OK:
			{
				//if OK then parsing request arguments
				byte[] data_arguments = Arrays.copyOfRange(data, 8, data.length);
				int begin_position = 0;
				int end_position = 4;
				int data_arg_size;
				byte[] argument;

				while (true)
				{
					data_arg_size = byteArrayToInt(Arrays.copyOfRange(data_arguments,
							begin_position,
							end_position));
					begin_position = end_position;
					end_position = begin_position + data_arg_size;

					argument = Arrays.copyOfRange(data_arguments, begin_position, end_position);
					message.arguments.add(new String(argument));

					begin_position = end_position;
					end_position += 4;

					if (begin_position >= data_arguments.length)
						break;
				}

			}
			break;

			case ERROR:
				// if error then the next byte sequence is an error message
				message.status = Message.Status.ERROR;
				message.err.code = status;
				int message_size = byteArrayToInt(Arrays.copyOfRange(data,
						4,
						8)); //until 12th character
				message.err.message = new String(Arrays.copyOfRange(data, 12, message_size));
		}
	}


	public byte[] generate_ok_response()
	{
		ByteBuffer buf;
		buf = ByteBuffer.allocate(4 + 4);
		buf.put(IntToByteArray(Message.Type.RESPONSE.value));
		buf.put(IntToByteArray(Message.Status.OK.value));

		return buf.array();
	}
}
