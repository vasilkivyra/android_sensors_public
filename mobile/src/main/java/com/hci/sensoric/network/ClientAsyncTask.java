package com.hci.sensoric.network;

/**
 * Created by yurii on 21/11/17.
 */

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Arrays;

/**
 * AsyncTask which handles the communication with the server
 */
public class ClientAsyncTask<T extends AsyncClientCallback> extends AsyncTask<AsyncTaskClientParams<T>, Void, Integer>
{
	public AsyncTaskClientParams<T> params;
	private byte[] message = new byte[1024 * 1024];
	private Socket socket;


	public ClientAsyncTask(AsyncTaskClientParams<T> params)
	{
		this.params = params;
	}

	public AsyncTaskClientParams<T> params()
	{
		return params;
	}

	@Override
	@SafeVarargs
	protected final Integer doInBackground(AsyncTaskClientParams<T>... params)
	{
		Log.d("Subscription Client: " + params[0].remote_host_address, "is STARTED!");

		try
		{
			if (params.length == 0)
				throw new RuntimeException("params cannot be empty");

			socket = new Socket(params[0].remote_host_address, params[0].port);
			BufferedInputStream in = new BufferedInputStream(socket.getInputStream());
			BufferedOutputStream out = new BufferedOutputStream(socket.getOutputStream());

			/* now we can control the lifespan of this async client thread */
			while (params[0].running)
			{
				//Log.d("Status: ","Working on task id #"+params[0].task_id);
				int length = 0;

				if ((length = params[0].callback.available(message)) > 0)
				{
					Log.d("Status: ",
							"Working on task id #" + params[0].task_id + ", bytes written = " + length);
					byte[] reply = Arrays.copyOfRange(message, 0, length);
					out.write(message_header_protocol.insert_header(reply));
					out.flush();
					//in.read();
				} else
				{
					//Log.d("Status: ", "Nothing is available");
				}

			}

			/* when running changes to false socket closes */
			socket.close();
			Log.d("Subscription Client: " + params[0].remote_host_address, "is EXITED!");


		}
		catch (NumberFormatException | IOException e)
		{
			e.printStackTrace();

			try
			{
				socket.close();
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}

		}
		return 0;
	}


}

