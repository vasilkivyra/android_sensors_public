package com.hci.sensoric.network;

import java.util.ArrayList;

/**
 * Created by yurii on 17/11/17.
 */

public class Message
{
	public Type type;
	public Method method;
	public Status status;
	public ArrayList<String> arguments = new ArrayList<>();
	public Error err = new Error();
	public SubscribeForRotationAngleEventsMethod subscribeForRotationAngleEventsMethod = new SubscribeForRotationAngleEventsMethod();
	public UnSubscribeForRotationAngleEventsMethod unsubscribeForRotationAngleEventsMethod = new UnSubscribeForRotationAngleEventsMethod();

	public enum Status
	{
		OK(0),
		ERROR(1);

		final int value;

		Status(int value)
		{
			this.value = value;
		}
	}

	public enum ErrorCode
	{
		INVALID_METHOD(100),
		INVALID_TYPE(101);

		final int value;

		ErrorCode(int v)
		{
			value = v;
		}
	}
	public enum Type
	{
		REQUEST(634),
		RESPONSE(635),
		INVALID(0x0);

		int value;

		Type(int value)
		{
			this.value = value;
		}
	}
	public enum Event
	{
		WRIST_ORIENTATION_UPDATE(2345),
		COUNT(2346);
		final int value;

		Event(int value)
		{
			this.value = value;
		}
	}
	public enum Method
	{
		SubscribeForRotationAngleEvents(1014),
		UnSubscribeForRotationAngleEvents(1015),
		SingleGetRotationAngleEvent(1016),
		ShortVibration(1017),
		LongVibration(1018),
		DoesNotExist(0);

		final int value;

		Method(int value)
		{
			this.value = value;
		}
	}

	public class SubscribeForRotationAngleEventsMethod
	{
		public short port;
		public String task_id;
	}

	public class UnSubscribeForRotationAngleEventsMethod
	{
		public String task_id;
	}

	public class Error
	{
		public int code = 0;
		public String message;
	}

}
