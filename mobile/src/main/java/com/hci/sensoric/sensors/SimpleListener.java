package com.hci.sensoric.sensors;

/**
 * Created by yurii on 22/11/17.
 */

public interface SimpleListener<T>
{
	public void update(T data);
}
