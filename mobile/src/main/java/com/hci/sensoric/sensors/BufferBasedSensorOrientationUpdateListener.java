package com.hci.sensoric.sensors;

import android.util.Log;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * Created by yurii on 22/11/17.
 */

public class BufferBasedSensorOrientationUpdateListener implements SimpleListener<long[]>
{
	private final Object mutex = new Object();
	private Queue<long[]> buffer = new ArrayDeque<>();

	@Override
	public void update(long[] data)
	{
		//Log.d("call(): ", "update(), buffer size = " + buffer.size() + ", this = " + this);
		synchronized (mutex)
		{
			buffer.add(data);
		}
	}

	protected boolean available(long[] container)
	{
		synchronized (mutex)
		{
			if (buffer.isEmpty())
				return false;
			else
			{
//				Log.d("call(): ",
//						"available(), buffer size = " + buffer.size() + ", this = " + this);
				long[] item = buffer.poll();
				if (item == null)
					return false;

				System.arraycopy(item, 0, container, 0, item.length);

				Log.d("Data: ", "" + container[0]);
				return true;
			}
		}
	}
}
